//= require jquery
//= require_tree .

$('.js-accordion-trigger').on('click', function(e){
  jQuery(this).parent().find('.first-menu').slideToggle('fast');  // apply the toggle to the ul
  jQuery(this).parent().toggleClass('is-expanded');
  e.preventDefault();
});

$('.js-inner-accordion-trigger').on('click', function(e){
  jQuery(this).parent().find('.second-menu').slideToggle('fast');  // apply the toggle to the ul
  jQuery(this).parent().toggleClass('is-expanded');
  e.preventDefault();
});
